#include "imagemodel.h"
#include <QDebug>

ImageModel::ImageModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int ImageModel::getLabelIndex(QString label) {
    int ret = -1;
    for (int i = 0; i < rowCount(); i++) {
        if (m_list[i].name == label) {
            return i;
        }
    }
    return ret;
}

int ImageModel::rowCount(const QModelIndex &parent) const{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid()) {
        return 0;
    }

    return m_list.size();
}

QVariant ImageModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid() || index.row() >= rowCount()) {
        return QVariant();
    }

    const auto &item = m_list[index.row()];

    switch (role) {
    case Qt::DisplayRole:
        return item.name;
    case Qt::DecorationRole:
        return item.thumbnail;
    }

    return {};
}

ImageObject ImageModel::rawData(const QModelIndex &index) {
    if (!index.isValid() || index.row() >= rowCount()) {
        return {};
    }
    return m_list[index.row()];
}

void ImageModel::addImage(QString name, QImage image) {
    int i = getLabelIndex(name);
    if (i == -1) {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_list << ImageObject{name, image, image.scaled(64, 64)};
        endInsertRows();
    } else {
        m_list[i] = ImageObject{name, image, image.scaled(64, 64, Qt::IgnoreAspectRatio, Qt::SmoothTransformation)};
        emit dataChanged(index(i), index(i), {});
    }
}
