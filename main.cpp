#include "mainwindow.h"
#include <QApplication>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setOrganizationName("FME VUT");
    QApplication::setOrganizationDomain("fme.vutbr.cz");
    QApplication::setApplicationName("ProteinDetector");

    Logic logic;
    MainWindow w(&logic);
    logic.init();
    w.show();

    return a.exec();
}
