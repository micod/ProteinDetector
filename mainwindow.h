#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>

#include "logic.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Logic *logic, QWidget *parent = nullptr);
    ~MainWindow();

    Logic* logic() const
    {
        return m_logic;
    }

public slots:
    void openFile();
    void selectDirectory();

signals:

private:
    Ui::MainWindow *ui;
    Logic *m_logic{};
    QtCharts::QChartView *m_chartView;
};

#endif // MAINWINDOW_H
