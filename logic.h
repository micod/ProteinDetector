#ifndef LOGIC_H
#define LOGIC_H

#include <QObject>
#include <QImage>
#include <QPointF>
#include <QList>
#include <QPair>

#include <opencv2/opencv.hpp>

#include "imagemodel.h"

class Logic : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QImage origImage READ origImage WRITE setOrigImage NOTIFY origImageChanged)
    Q_PROPERTY(QImage processedImage READ processedImage WRITE setProcessedImage NOTIFY processedImageChanged)
    Q_PROPERTY(int lastSelected READ lastSelected WRITE setLastSelected NOTIFY lastSelectedChanged)
    Q_PROPERTY(int topHatWidth READ topHatWidth WRITE setTopHatWidth NOTIFY topHatWidthChanged)
    Q_PROPERTY(int threshold READ threshold WRITE setThreshold NOTIFY thresholdChanged)
    Q_PROPERTY(int maskWidth READ maskWidth WRITE setMaskWidth NOTIFY maskWidthChanged)
    Q_PROPERTY(int maskX READ maskX WRITE setMaskX NOTIFY maskXChanged)
    Q_PROPERTY(int maskY READ maskY WRITE setMaskY NOTIFY maskYChanged)
    Q_PROPERTY(QString imagePath READ imagePath WRITE setImagePath NOTIFY imagePathChanged)

    ImageModel *m_processModel;

    QImage m_origImage;

    QImage m_processedImage;

    QImage matToImage(cv::Mat mat);

    int m_threshold{};

    int m_lastSelected{-1};

    int m_topHatWidth{3};

    int m_maskWidth{0};

    int m_maskX{0};

    int m_maskY{0};

    QList<QPointF> granulometry(cv::Mat foreground);

    QString m_imagePath;

public:
    explicit Logic(QObject *parent = nullptr);
    ~Logic();

    QImage origImage() const
    {
        return m_origImage;
    }

    QImage processedImage() const
    {
        return m_processedImage;
    }

    ImageModel *processModel() const;

    int threshold() const
    {
        return m_threshold;
    }

    int lastSelected() const
    {
        return m_lastSelected;
    }

    int topHatWidth() const
    {
        return m_topHatWidth;
    }

    int maskWidth() const
    {
        return m_maskWidth;
    }

    int maskX() const
    {
        return m_maskX;
    }

    int maskY() const
    {
        return m_maskY;
    }

    QString imagePath() const
    {
        return m_imagePath;
    }

signals:
    void origImageChanged(QImage origImage);
    void processedImageChanged(QImage processedImage);
    void thresholdChanged(int threshold);
    void lastSelectedChanged(int lastSelected);
    void topHatWidthChanged(int topHatWidth);
    void maskWidthChanged(int maskWidth);
    void maskXChanged(int maskX);
    void maskYChanged(int maskY);
    void newImageDimensions(QSize size);
    void granulometryMesurements(QList<QPointF> grains);
    void imagePathChanged(QString imagePath);
    void imageStats(int count, int avgSize);

public slots:
    void init();
    QPair<int,int> process(QString file = QString());
    void onProcessListClicked(QModelIndex index);
    void loadSettings();
    void saveSettings();
    void processDirectory(QString path);

    void setOrigImage(QImage origImage)
    {
        m_origImage = origImage;
        emit origImageChanged(m_origImage);
    }
    void setProcessedImage(QImage processedImage)
    {
        m_processedImage = processedImage;
        emit processedImageChanged(m_processedImage);
    }
    void setThreshold(int threshold)
    {
        if (m_threshold == threshold)
            return;

        m_threshold = threshold;
        emit thresholdChanged(m_threshold);
    }
    void setLastSelected(int lastSelected)
    {
        if (m_lastSelected == lastSelected)
            return;

        m_lastSelected = lastSelected;
        emit lastSelectedChanged(m_lastSelected);
    }
    void setTopHatWidth(int topHatWidth)
    {
        if (m_topHatWidth == topHatWidth)
            return;

        m_topHatWidth = topHatWidth;
        emit topHatWidthChanged(m_topHatWidth);
    }
    void setMaskWidth(int maskWidth)
    {
        if (m_maskWidth == maskWidth)
            return;

        m_maskWidth = maskWidth;
        emit maskWidthChanged(m_maskWidth);
    }
    void setMaskX(int maskX)
    {
        if (m_maskX == maskX)
            return;

        m_maskX = maskX;
        emit maskXChanged(m_maskX);
    }
    void setMaskY(int maskY)
    {
        if (m_maskY == maskY)
            return;

        m_maskY = maskY;
        emit maskYChanged(m_maskY);
    }
    void setImagePath(QString imagePath)
    {
        if (m_imagePath == imagePath)
            return;

        m_imagePath = imagePath;
        emit imagePathChanged(m_imagePath);
    }
};

#endif // LOGIC_H
