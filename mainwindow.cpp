#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAction>
#include <QFileDialog>

#include <algorithm>

MainWindow::MainWindow(Logic *logic, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_logic(logic)
{
    ui->setupUi(this);

    QToolBar *toolbar = addToolBar("File");
    toolbar->setMovable(false);
    QAction *openFileAction = toolbar->addAction("Image...");
    connect(openFileAction, &QAction::triggered, this, &MainWindow::openFile);
    QAction *processDirAction = toolbar->addAction("Folder...");
    connect(processDirAction, &QAction::triggered, this, &MainWindow::selectDirectory);

    m_chartView = new QtCharts::QChartView(this);
    m_chartView->setRenderHint(QPainter::Antialiasing);
    ui->verticalLayout->addWidget(m_chartView);

//    ui->processedLabel->setMinimumSize(1,1);
    ui->processView->setModel(logic->processModel());
    connect(m_logic, &Logic::processedImageChanged, this, [this](QImage image) {
        ui->processedLabel->setPixmap(QPixmap::fromImage(image));
    });
    connect(m_logic, &Logic::newImageDimensions, this, [this](QSize size) {
        ui->maskWidthSpinBox->setMaximum(std::max(2*size.width(), 2*size.height()));
        ui->maskCenterXSpinBox->setMaximum(size.width()-1);
        ui->maskCenterYSpinBox->setMaximum(size.height()-1);
    });
    connect(m_logic, &Logic::granulometryMesurements, this, [this](QList<QPointF> grains) {
        auto *lineSeries = new QtCharts::QLineSeries();
        auto *chart = new QtCharts::QChart();
        for (const auto &point : grains) {
            *lineSeries << point;
        }
        chart->addSeries(lineSeries);
        chart->legend()->hide();
        chart->setTitle("Granulometry");
        chart->createDefaultAxes();
        m_chartView->setChart(chart);
        m_chartView->update();
    });
    connect(m_logic, &Logic::imageStats, this, [this](int count, int avgSize) {
        ui->statsLabel->setText(QString("Count: %1\nAverage size: %2").arg(count).arg(avgSize));
    });

    // ui -> logic
    connect(ui->processView, &QListView::clicked, logic, &Logic::onProcessListClicked);
    connect(ui->topHatSpinBox, SIGNAL(valueChanged(int)), logic, SLOT(setTopHatWidth(int)));
    connect(ui->thresholdSpinBox, SIGNAL(valueChanged(int)), logic, SLOT(setThreshold(int)));
    connect(ui->maskWidthSpinBox, SIGNAL(valueChanged(int)), logic, SLOT(setMaskWidth(int)));
    connect(ui->maskCenterXSpinBox, SIGNAL(valueChanged(int)), logic, SLOT(setMaskX(int)));
    connect(ui->maskCenterYSpinBox, SIGNAL(valueChanged(int)), logic, SLOT(setMaskY(int)));

    // logic -> ui
    connect(m_logic, &Logic::thresholdChanged, ui->thresholdSpinBox, &QSpinBox::setValue);
    connect(m_logic, &Logic::topHatWidthChanged, ui->topHatSpinBox, &QSpinBox::setValue);
    connect(m_logic, &Logic::maskWidthChanged, ui->maskWidthSpinBox, &QSpinBox::setValue);
    connect(m_logic, &Logic::maskXChanged, ui->maskCenterXSpinBox, &QSpinBox::setValue);
    connect(m_logic, &Logic::maskYChanged, ui->maskCenterYSpinBox, &QSpinBox::setValue);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile() {
    QString path = QFileDialog::getOpenFileName(this, "Open image...");
    if (!path.isNull()) {
        m_logic->setImagePath(path);
    }
}

void MainWindow::selectDirectory() {
    QString dirPath = QFileDialog::getExistingDirectory(this, "Select directory");
    m_logic->processDirectory(dirPath);
}
