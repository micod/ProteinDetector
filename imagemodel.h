#ifndef IMAGEMODEL_H
#define IMAGEMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QImage>

struct ImageObject {
    QString name;
    QImage image;
    QImage thumbnail;
};

class ImageModel : public QAbstractListModel
{
    Q_OBJECT
    QList<ImageObject> m_list;
    int getLabelIndex(QString label);

public:
    explicit ImageModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    ImageObject rawData(const QModelIndex &index);
    void addImage(QString name, QImage image);

private:
};

#endif // IMAGEMODEL_H
