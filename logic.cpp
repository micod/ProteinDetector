#include "logic.h"

#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QFile>

QImage Logic::matToImage(cv::Mat mat) {
    QImage::Format imageFormat = QImage::Format_Invalid;
    switch (mat.type()) {
    case CV_8UC3:
        imageFormat = QImage::Format_RGB888;
        break;
    case CV_8UC1:
        imageFormat = QImage::Format_Grayscale8;
        break;
    default:
        qDebug() << "unsupported cv::Mat type:" << mat.type();
        return {};
    }
    QImage image(mat.cols, mat.rows, imageFormat);
    std::copy(static_cast<const uchar*>(mat.data), mat.dataend, image.bits());
    return image;
}

QList<QPointF> Logic::granulometry(cv::Mat threshold) {
    cv::Mat labels;
    cv::Mat opening;
    cv::Mat foreground;
    QList<QPointF> grains;
    threshold.copyTo(foreground);
    int prevCount = cv::connectedComponents(foreground, labels, 4, CV_16UC1)-1;

    for (int i = 3; ; i++) {
        cv::Mat se = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(i,i));
        cv::morphologyEx(foreground, opening, cv::MORPH_OPEN, se);
        int count = cv::connectedComponents(foreground, labels, 4, CV_16UC1);
        grains << QPointF(i,prevCount-(count-1));
        prevCount = count;
        if (count == 1) {
            return grains;
        }
        opening.copyTo(foreground);
    }
}

Logic::Logic(QObject *parent) : QObject(parent) {
    m_processModel = new ImageModel(this);
    connect(this, &Logic::origImageChanged, this, [this](QImage image) { emit newImageDimensions(image.size()); });
    connect(this, &Logic::origImageChanged, this, [this]() {
        process();
    });
    connect(this, &Logic::topHatWidthChanged, this, [this]() {
        process();
    });
    connect(this, &Logic::thresholdChanged, this, [this]() {
        process();
    });
    connect(this, &Logic::maskWidthChanged, this, [this]() {
        process();
    });
    connect(this, &Logic::maskXChanged, this, [this]() {
        process();
    });
    connect(this, &Logic::maskYChanged, this, [this]() {
        process();
    });
    connect(this, &Logic::imagePathChanged, this, [this](QString path) {
        QImage image = QImage(path).convertToFormat(QImage::Format_RGB888);
        setOrigImage(image);
    });
}

Logic::~Logic() {
    saveSettings();
}

void Logic::init() {
    loadSettings();
}

QPair<int,int> Logic::process(QString file) {
    if (origImage().isNull() && file.isNull()) {
        return {};
    }
    QImage usedImage;
    if (!file.isNull()) {
        usedImage = QImage(file).convertToFormat(QImage::Format_RGB888);
    } else {
        usedImage = origImage();
    }
    cv::Mat orig(usedImage.height(), usedImage.width(), CV_8UC3);
    std::copy(usedImage.constBits(), usedImage.constBits()+usedImage.sizeInBytes(), orig.data);
    cv::Mat grayscale;
    cv::cvtColor(orig, grayscale, CV_RGB2GRAY);

    // mask
    cv::Mat maskOutline;
    cv::Mat maskedGreyscale;
    if (maskWidth() == 0 || maskX() >= usedImage.width() || maskY() >= usedImage.height()) {
        maskedGreyscale = grayscale;
    } else {
        orig.copyTo(maskOutline);
        cv::circle(maskOutline, cv::Point(maskX(), maskY()), maskWidth(), cv::Scalar_<uchar>(0, 255, 0), 1);

        cv::Mat mask = cv::Mat::zeros(usedImage.height(), usedImage.width(), CV_8UC1);
        cv::circle(mask, cv::Point(maskX(), maskY()), maskWidth(), cv::Scalar_<uchar>(255), -1);
        grayscale.copyTo(maskedGreyscale, mask);
    }

    // background subtractions
    cv::Mat topHat;
    cv::morphologyEx(maskedGreyscale, topHat, cv::MORPH_TOPHAT, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(topHatWidth(),topHatWidth())));

    // thresholding
    cv::Mat thresh;
    cv::threshold(topHat, thresh, threshold(), 255, cv::THRESH_BINARY);

    // outline
    cv::Mat dilated;
    cv::dilate(thresh, dilated, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3)));
    cv::Mat outline;
    cv::subtract(dilated, thresh, outline);
    cv::Mat coloredOutline;
    cv::Mat greenChannel = cv::Mat::zeros(outline.rows, outline.cols, CV_8UC1);
    cv::Mat blueChannel = cv::Mat::zeros(outline.rows, outline.cols, CV_8UC1);
    cv::Mat channels[] = {outline, greenChannel, blueChannel};
    cv::merge(channels, 3, coloredOutline);
    cv::Mat overlay;
    cv::add(coloredOutline, orig, overlay);

//    // labeling
    cv::Mat labels;
    cv::Mat stats;
    cv::Mat centroids;
    int count = cv::connectedComponentsWithStats(thresh, labels, stats, centroids, 4, CV_16UC1);

    int avgSize = 0;
    for (int i = 1; i < count; i++) {
        avgSize += stats.at<int>(i, cv::CC_STAT_AREA);
    }
    if (count > 1) {
        avgSize /= (count-1);
    }

    if (file.isNull()) {
        emit imageStats(count-1, avgSize);

        auto grains = granulometry(thresh);
        emit granulometryMesurements(grains);

        QImage tophatImage = matToImage(topHat);
        QImage threshImage = matToImage(thresh);
        QImage overlayImage = matToImage(overlay);

        m_processModel->addImage("original", matToImage(orig));
        m_processModel->addImage("mask outline", matToImage(maskOutline));
        m_processModel->addImage("tophat", tophatImage);
        m_processModel->addImage("threshold", threshImage);
        //    m_processModel->addImage("outline", matToImage(outline));
        //    m_processModel->addImage("colored", matToImage(coloredOutline));
        m_processModel->addImage("overlay", overlayImage);

        tophatImage.save("tophat.png");
        threshImage.save("threshold.png");
        overlayImage.save("overlay.png");

        setProcessedImage(m_processModel->rawData(m_processModel->index(lastSelected())).image);
    }

    //    cv::cvtColor(overlay, overlay, CV_RGB2BGR);
    //    cv::imwrite("overlay.png", overlay);

    return {count-1, avgSize};
}

void Logic::onProcessListClicked(QModelIndex index) {
    QImage modelImage = m_processModel->rawData(index).image;
    setProcessedImage(modelImage);
    setLastSelected(index.row());
}

void Logic::loadSettings() {
    QSettings s;;
    setTopHatWidth(s.value("tophat", 3).toInt());
    setThreshold(s.value("threshold").toInt());
    setMaskWidth(s.value("maskWidth").toInt());
    setMaskX(s.value("maskX").toInt());
    setMaskY(s.value("maskY").toInt());
    setImagePath(s.value("imagePath").toString());
}

void Logic::saveSettings() {
    QSettings s;
    s.setValue("tophat", topHatWidth());
    s.setValue("threshold", threshold());
    s.setValue("maskWidth", maskWidth());
    s.setValue("maskX", maskX());
    s.setValue("maskY", maskY());
    s.setValue("imagePath", imagePath());
}

void Logic::processDirectory(QString path) {
    QDir dir(path);
    QStringList files = dir.entryList(QDir::Files);
    QString csvFileName = path + QDir::separator() + "result.csv";
    files.removeOne("result.csv");
    QFile csvFile(csvFileName);
    bool ok = csvFile.open(QIODevice::WriteOnly);
    if (!ok) {
        qDebug() << "[ERROR]\tCouldn't open result file" << csvFileName;
        return;
    }
    int i = 0;
    for (QString file : files) {
        QPair<int,int> stats = process(path+QDir::separator()+file);
        QString writeString = QString("%1,%2,%3\n").arg(file).arg(QString::number(stats.first)).arg(QString::number(stats.second));
        csvFile.write(writeString.toStdString().c_str());
        csvFile.flush();
        qDebug() << "processed" << QString("%1/%2").arg(QString::number(++i)).arg(files.size()) << file;
    }
    csvFile.close();
    qDebug() << "all done";
}

ImageModel *Logic::processModel() const {
    return m_processModel;
}
